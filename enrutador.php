<?php
    require_once ('library/operaciones.php');
    require_once ('library/pi.php');

    $accion = $_GET['hacer'];

    if ($accion == '') {
        // Ir al home
    } else {
        // suma/1/2 --> ['suma', 1, 2]
        $parametros = explode('/', $accion);

        switch ($parametros[0]) {
            case 'sumar': $resultado = sumar($parametros[1], $parametros[2]); break;
            case 'restar': $resultado = restar($parametros[1], $parametros[2]); break;
            case 'multiplicar': $resultado = multiplicar($parametros[1], $parametros[2]); break;
            case 'dividir': $resultado = dividir($parametros[1], $parametros[2]); break;
            case 'pi': mostrarPi(); break;
            case 'about': ; break;
            default : echo('Operacion desconocida'); break;
        }

        if (isset($resultado)) {
            echo('El resultado es:'.$resultado);
        }
    }
   

?>